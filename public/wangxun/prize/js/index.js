layui.use('table', function(){
    var table = layui.table;
    //方法级渲染
    table.render({
        elem: '#LAY_table_user'
        ,url: 'prize_list'
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,cols: [[
            {checkbox: true, fixed: true}
            ,{field:'id', title: 'ID', sort: true, fixed: true}
            ,{field:'prize_name', title: '奖品'}
            ,{field:'integral', title: '兑换需要积分'}
            ,{field:'prize_img_url', templet:function(data){
                    return '<img style="width: 300px;height: 300px;" src="' + data.prize_img_url + '">'
                },title: '图片'}
            ,{field:'explain', title: '简介'}
            ,{field:'time', title: '创建时间', sort: true}
            ,{field:'updated_at', title: '最近修改时间', sort: true}
            ,{field:'', title: '操作', toolbar: '#bar'}
        ]]
        ,id: 'testReload'
        ,page: true
    });

    //监听工具条
    table.on('tool(user)', function(obj){           // user 指的是页面中 lay-filter="user"的user
        var data = obj.data;
        if(obj.event === 'detail'){
            layer.msg('ID：'+ data.id + ' 的查看操作');
        } else if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                $.ajax({
                    type: "POST",
                    data: {'id':data.id},
                    url:'prize_del',
                    dataType: "json",
                    success: function(data){
                        if (0 != data.code) {
                            layer.alert(data.msg + ',错误码:'+ data.code);
                        } else {
                            layer.open({
                                content: '删除成功',
                                yes: function(){
                                    obj.del();
                                    window.location.href = "prize_index";
                                }
                            });
                        }
                    }
                });
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            //var data = JSON.stringify(data);
           // layer.alert('编辑行：<br>'+ JSON.stringify(data));
            window.location.href =  "prize_edit?id=" + data.id ;
        }
    });

    var $ = layui.$, active = {
        reload: function(){
            var name = $('#name').val();
            var uid = $('#uid').val();
            //执行重载
            table.reload('testReload', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                ,where: {
                    uid: uid,
                    name: name
                }
            });
        }
    };

    $('#export').on('click', function(){
        var name = $('#name').val();
        var uid = $('#uid').val();
        window.location.href = export_url + "?name=" + name + "&uid=" + uid;
    });

    $('.search .layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
});
