layui.use(['form', 'layedit', 'laydate','flow'], function(){
    var form = layui.form
        ,layer = layui.layer
        ,layedit = layui.layedit
        ,flow = layui.flow ;

    //创建一个编辑器
    var editIndex = layedit.build('LAY_demo_editor');

    //自定义验证规则
    form.verify({
        content: function(value){
            layedit.sync(editIndex);
        }
    });

    //监听提交
    form.on('submit(mycommit)', function(data){
        var data_obj = data.field;
        $.ajax({
            type: "post",
            url: "title_save",
            data: data_obj,
            dataType: "json",
            success: function(data){
               if (0 != data.code) {
                   layer.alert(data.msg + ',错误码:'+ data.code);
               } else {
                   layer.open({
                       content: '新增成功',
                       yes: function(){
                           window.location.href = "title_index";
                       }
                   });
               }
            }
        });
        return false;
    });
});


