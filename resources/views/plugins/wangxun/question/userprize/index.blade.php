@extends('layouts.app')

@section('content')
    <!-- 面板 -->
    <blockquote class="layui-elem-quote top-title"><h3>用户兑换奖品列表</h3></blockquote>

    <!-- 搜索 -->
    <div class="search">
    </div>

    <!-- 表格 -->
    <table class="layui-hide" id="LAY_table_user" lay-filter="user"></table>

    <!-- 操作 -->
    <script type="text/html" id="bar">
    </script>

    <!-- js -->
    <script src="{{asset('/wangxun/userprize/js/index.js')}}?v=1006"></script>

@endsection
