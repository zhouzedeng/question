@extends('layouts.app')

@section('content')
    <!-- 面板 -->
    <blockquote class="layui-elem-quote top-title"><h3><a onclick="history.back()">称号管理</a> / 添加称号</h3></blockquote>
    <form class="layui-form">

        <div class="layui-form-item">
            <label class="layui-form-label">最小分数</label>
            <div class="layui-input-block">
                <input  name="score_min" lay-verify="price" autocomplete="off" placeholder="请输入最大分数" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">最大分数</label>
            <div class="layui-input-block">
                <input  name="score_max" lay-verify="price" autocomplete="off" placeholder="请输入最小分数" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">称号</label>
            <div class="layui-input-block">
                <input  name="title" lay-verify="price" autocomplete="off" placeholder="请输入称号" class="layui-input" >
            </div>
        </div>




        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="mycommit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

    </form>

    <script>
    </script>
    <script src="{{asset('/wangxun/title/js/add.js')}}?v=510090000000"></script>

@endsection
