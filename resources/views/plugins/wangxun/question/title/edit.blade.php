@extends('layouts.app')

@section('content')
    <!-- 面板 -->
    <blockquote class="layui-elem-quote top-title"><h3><a onclick="history.back()">称号管理</a> / 修改称号</h3></blockquote>
    <form class="layui-form">
       {{--// <input type="hidden" name="_token" value="{{csrf_token()}}">--}}
        <input type="hidden" name="id" id="id" value="{{$title->id}}" >
        <div class="layui-form-item">
            <label class="layui-form-label">最小分数</label>
            <div class="layui-input-block">
                <input type="text" name="score_min" lay-verify="name" value="{{$title->score_min}}" autocomplete="off" placeholder="请输入小分数" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">最大分数</label>
            <div class="layui-input-block">
                <input type="text" name="score_max" lay-verify="name" value="{{$title->score_max}}" autocomplete="off" placeholder="请输入大分数" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">称号</label>
            <div class="layui-input-block">
                <input type="text" name="title" lay-verify="name" value="{{$title->title}}" autocomplete="off" placeholder="请输入称号" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="mycommit">提交修改</button>
                <button  class="layui-btn layui-btn-primary"><a href="title_index">返回</a></button>
            </div>
        </div>

    </form>

    <script>
    </script>
    <script src="{{asset('/wangxun/title/js/edit.js')}}?v=19000"></script>

@endsection
