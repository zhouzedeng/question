@extends('layouts.app')

@section('content')
    <!-- 面板 -->
    <blockquote class="layui-elem-quote top-title"><h3><a onclick="history.back()">题目管理</a> / 添加题目</h3></blockquote>
    <form class="layui-form">

        <div class="layui-form-item">
            <label class="layui-form-label">题目内容</label>
            <div class="layui-input-block">
                <input  name="question" lay-verify="name" autocomplete="off" placeholder="请输入题目内容" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项一</label>
            <div class="layui-input-block">
                <input  name="option_1" lay-verify="price" autocomplete="off" placeholder="请输入选项一" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项二</label>
            <div class="layui-input-block">
                <input  name="option_2" lay-verify="price" autocomplete="off" placeholder="请输入选项二" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项三</label>
            <div class="layui-input-block">
                <input  name="option_3" lay-verify="price" autocomplete="off" placeholder="请输入选项三" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项四</label>
            <div class="layui-input-block">
                <input  name="option_4" lay-verify="price" autocomplete="off" placeholder="请输入选项四" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">答案</label>
            <div class="layui-input-block">
                <input  name="answer" lay-verify="need_cut_num" autocomplete="off" placeholder="请输入答案" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="mycommit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

    </form>

    <script>
    </script>
    <script src="{{asset('/wangxun/question/js/add.js')}}?v=51009000000"></script>

@endsection
