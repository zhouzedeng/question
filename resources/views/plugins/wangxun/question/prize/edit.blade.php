@extends('layouts.app')

@section('content')
    <!-- 面板 -->
    <blockquote class="layui-elem-quote top-title"><h3><a onclick="history.back()">奖品管理</a> / 修改奖品</h3></blockquote>
    <form class="layui-form">
        {{--// <input type="hidden" name="_token" value="{{csrf_token()}}">--}}
        <input type="hidden" name="id" id="id" value="{{$prize->id}}" >
        <div class="layui-form-item">
            <label class="layui-form-label">奖品名称</label>
            <div class="layui-input-block">
                <input type="text" name="prize_name" lay-verify="name" value="{{$prize->prize_name}}" autocomplete="off" placeholder="请输入奖品名称" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">兑换需要分数</label>
            <div class="layui-input-block">
                <input type="text" name="integral" lay-verify="price" value="{{$prize->integral}}" autocomplete="off" placeholder="请输入兑换需要分数" class="layui-input" >
            </div>
        </div>

        <input id="img" type="hidden" name="prize_img_url" lay-verify="img" value="{{$prize->prize_img_url}}">
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">奖品图片</label>
            <div class="layui-input-block">
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-normal" id="testList">选择文件</button>
                    <button type="button" class="layui-btn" id="testListAction">开始上传</button>
                    <div class="layui-upload-list">
                        <table class="layui-table">
                            <thead>
                            <tr><th>文件名</th>
                                <th>大小</th>
                                <th>预览图</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                            <tr class="storage_goods_img">
                                <td></td>
                                <td></td>
                                <td><img src="{{$prize->prize_img_url}}"></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody id="demoList"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">简介</label>
            <div class="layui-input-block">
                <input type="text" name="explain" lay-verify="price" value="{{$prize->explain}}" autocomplete="off" placeholder="请输入简介" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="mycommit">提交修改</button>
                <button  class="layui-btn layui-btn-primary"><a href="prize_index">返回</a></button>
            </div>
        </div>

    </form>

    <script>
    </script>
    <script src="{{asset('/wangxun/prize/js/edit.js')}}?v=19000"></script>

@endsection
