@extends('layouts.app')

@section('content')
    <!-- 面板 -->
    <blockquote class="layui-elem-quote top-title"><h3><a onclick="history.back()">题目管理</a> / 修改题目</h3></blockquote>
    <form class="layui-form">
       {{--// <input type="hidden" name="_token" value="{{csrf_token()}}">--}}
        <input type="hidden" name="id" id="id" value="{{$question->id}}" >
        <div class="layui-form-item">
            <label class="layui-form-label">题目内容</label>
            <div class="layui-input-block">
                <input type="text" name="question" lay-verify="name" value="{{$question->question}}" autocomplete="off" placeholder="请输入题目内容" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项一</label>
            <div class="layui-input-block">
                <input type="text" name="option_1" lay-verify="price" value="{{$question->option_1}}" autocomplete="off" placeholder="请输入问题选项一" class="layui-input" >
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">选项二</label>
            <div class="layui-input-block">
                <input type="text" name="option_2" lay-verify="price" value="{{$question->option_2}}" autocomplete="off" placeholder="请输入问题选项二" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项三</label>
            <div class="layui-input-block">
                <input type="text" name="option_3" lay-verify="price" value="{{$question->option_3}}" autocomplete="off" placeholder="请输入问题选项三" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项四</label>
            <div class="layui-input-block">
                <input type="text" name="option_4" lay-verify="price" value="{{$question->option_4}}" autocomplete="off" placeholder="请输入问题选项四" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">答案</label>
            <div class="layui-input-block">
                <input type="text" name="answer" lay-verify="price" value="{{$question->answer}}" autocomplete="off" placeholder="请输入问题答案" class="layui-input" >
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="mycommit">提交修改</button>
                <button  class="layui-btn layui-btn-primary"><a href="question_index">返回</a></button>
            </div>
        </div>

    </form>

    <script>
    </script>
    <script src="{{asset('/wangxun/question/js/edit.js')}}?v=19000"></script>

@endsection
