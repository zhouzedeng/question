<?php

Route::post('/upload_upload', 'UploadController@upload')->name('upload.upload');
Route::get('/thirdApi_getCouponInfo', 'ThridApiController@getCouponInfo')->name('thirdApi.getCouponInfo');
Route::get('/thirdApi_sendSms', 'ThridApiController@sendSms')->name('thirdApi.sendSms');
Route::get('/home_index', 'HomeController@index')->name('home.index');
/*********************/
// 活动模块
Route::get('/activity_index', 'ActivityController@index')->name('activity.index');
Route::get('/activity_list', 'ActivityController@getList')->name('activity.list');
Route::get('/activity_add', 'ActivityController@add')->name('activity.add');
Route::POST('/activity_save', 'ActivityController@save')->name('activity.save');
Route::POST('/activity_del', 'ActivityController@del')->name('activity.del');
Route::POST('/activity_edit', 'ActivityController@edit')->name('activity.edit');
Route::get('/activity_edit', 'ActivityController@edit')->name('activity.edit');
Route::get('/fing_activity_goods', 'ActivityController@fingActivityGoods')->name('activity.fingActivityGoods');

// 用户模块
Route::get('/user_index', 'UserController@index')->name('user.index');
Route::get('/user_list', 'UserController@getList')->name('user.list');

// 访客模块
Route::get('/visitor_index', 'VisitorController@index')->name('visitor.index');
Route::get('/visitor_list', 'VisitorController@getList')->name('visitor.list');

/********************/

// 审核模块
Route::get('/verify_index', 'VerifyController@index')->name('verify.index');
Route::get('/verify_list', 'VerifyController@getList')->name('verify.list');
Route::post('/verify_check', 'VerifyController@check')->name('verify.check');

Route::get('/user_allIndex', 'UserController@allIndex')->name('user.allIndex');
Route::get('/user_allList', 'UserController@allList')->name('user.allList');

// 访客模块
Route::get('/visitor_allIndex', 'VisitorController@allIndex')->name('visitor.allIndex');
Route::get('/visitor_allList', 'VisitorController@allList')->name('visitor.allList');

/**********/

// 砍价接口
Route::get('/api_adduser', 'ApiController@adduser')->name('api.adduser');
Route::get('/api_getAllSellerGoods', 'ApiController@getAllSellerGoods')->name('api.getAllSellerGoods');
Route::get('/api_getaddGoodsToCut', 'ApiController@addGoodsToCut')->name('api.addGoodsToCut');
Route::get('/api_getCutInfo', 'ApiController@getCutInfo')->name('api.getCutInfo');
Route::get('/api_getCutVisitor', 'ApiController@getCutVisitor')->name('api.getCutVisitor');
Route::get('/api_cut', 'ApiController@cut')->name('api.cut');
Route::get('/thirdApi_getCarSeriesInfo', 'ThridApiController@getCarSeriesInfo')->name('thirdApi.getCarSeriesInfo');
Route::get('/thirdApi_sendSmsCode', 'ThridApiController@sendSmsCode')->name('thirdApi.sendSmsCode');
Route::get('/api_getActivity', 'ApiController@getActivity')->name('api.getActivity');
Route::get('/api_getSeries', 'ApiController@getSeries')->name('api.getSeries');
Route::get('/api_getShareInfo', 'ThridApiController@shaerInfo')->name('thirdApi.shaerInfo');
/**********/


/********************************************************************************************************************/

//题目管理模块
Route::get('/question_list', 'QuestionController@getList')->name('question.list');
Route::POST('/question_del', 'QuestionController@del')->name('question.del');
Route::get('/question_index', 'QuestionController@index')->name('question.index');
Route::get('/question_edit', 'QuestionController@edit')->name('question.edit');
Route::POST('/question_save', 'QuestionController@save')->name('question.save');
Route::get('/question_add', 'QuestionController@add')->name('question.add');
Route::get('/find_question_series', 'QuestionController@findQuestionSeries')->name('question.find_question_series');
Route::post('/question_edit', 'QuestionController@edit')->name('question.edit');

//答题兑换奖品
Route::get('/getprize_index', 'UserPrizeController@index')->name('getprize.index');
Route::get('/getprize_list', 'UserPrizeController@getList')->name('getprize.list');
Route::get('/prize_add', 'PrizeController@add')->name('prize.add');
Route::post('/prize_add', 'PrizeController@add')->name('prize.add');
Route::get('/prize_edit', 'PrizeController@edit')->name('prize.edit');
Route::post('/prize_edit', 'PrizeController@edit')->name('prize.edit');

//答题奖品管理列表
Route::get('/prize_list', 'PrizeController@getList')->name('prize.list');
Route::POST('/prize_del', 'PrizeController@del')->name('prize.del');
Route::get('/prize_index', 'PrizeController@index')->name('prize.index');

//称号管理
Route::get('/title_index', 'TitleController@index')->name('title.index');
Route::get('/title_list', 'TitleController@getList')->name('title.list');
Route::get('/title_edit', 'TitleController@edit')->name('title.edit');
Route::post('/title_edit', 'TitleController@edit')->name('title.edit');
Route::post('/title_save', 'TitleController@save')->name('title.save');
Route::get('/title_add', 'TitleController@add')->name('title.add');
Route::POST('/title_del', 'TitleController@del')->name('title.del');

// 答题接口
Route::get('/api_getMyPrizeList', 'ApiController@getMyPrizeList')->name('api.getMyPrizeList');
Route::get('/api_getQuestion', 'ApiController@getQuestion')->name('api.getQuestion');
Route::get('/api_getPrizeList', 'ApiController@getPrizeList')->name('api.getPrizeList');
Route::get('/api_getPrize', 'ApiController@getPrize')->name('api.getPrize');
Route::get('/api_getExchange', 'ApiController@getExchange')->name('api.getExchange');
Route::get('/question_list', 'QuestionController@getList')->name('question.list');







