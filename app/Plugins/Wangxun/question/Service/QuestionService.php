<?php
namespace Wangxun\Question\Service;

use Wangxun\Question\Model\Question;


/**
 * 答题业务
 * Class QuestionService
 * @package Wangxun\Common\Service
 * @author guang
 * @since 2018-11-28
 */
class QuestionService extends BaseService
{
    /**
     * 获取题目列表数据
     * @param array $data
     * @return array
     * @author yanguang
     * @since 2018-11-23
     */
    public static function getList($data = [])
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());

        // 查询数据
        $param = [];
        $order = array('id' , 'desc');
        $param['deltime'] = 0;
        $list = Question::getListByParam($param, $data['page'], $data['limit'], null, $order);
        $total = Question::getCntByParam($param);
        foreach ($list as $k => $v) {

            $list[$k]->time = date("Y-m-d H:i:s", $v->time);
        }
        $result['data'] = $list;
        $result['count'] = $total;
        return $result;
    }

    /**
     * 新增答题数据
     * @param array $params
     * @return array
     * @author guang
     * @since 2018-11-1
     */
    public static function save($params = array())
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        $data = [
            'question' => $params['question'],
            'answer' => $params['answer'],
            'option_1' => $params['option_1'],
            'option_2' => $params['option_2'],
            'option_3' => $params['option_3'],
            'option_4' => $params['option_4'],
            'time' => time(),
            'updated_at' => time()
        ];
        $rs = Question::add($data);
        if (empty($rs)) {
            $result['code'] = '200001';
            $result['msg'] = '添加失败';
        }
        return $result;
    }

    /**
     * 删除商品数据
     * @param array $params
     * @return array
     * @author shengquan
     * @since 2018-11-1
     */
    public static function del($params = array())
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        $data ['deltime'] = time();
        $rs = Question::updateById($data,$params['id']);
        if (empty($rs)) {
            $result['code'] = '100001';
            $result['msg'] = '删除失败';
        }
        return $result;
    }

    /**
     * 获取一条题目数据
     * @param array $data
     * @return array
     * @author shengquan
     * @since 2018-11-1
     */
    public static function getFind($params = [])
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        // 查询数据
        $where = [];
        $where ['id'] = $params ['id'];
        $find = Question::getOneByParam($where,'*');
        $result['data'] = $find;
        return $result;
    }

    /**
     * 修改题目数据
     * @param array $params
     * @return array
     * @author guang
     * @since 2018-11-1
     */
    public static function updata_question($params = array())
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        $data = [
            'question' => $params['question'],
            'answer' => $params['answer'],
            'option_1' => $params['option_1'],
            'option_2' => $params['option_2'],
            'option_3' => $params['option_3'],
            'option_4' => $params['option_4'],
            'updated_at' => time()
        ];
        $rs = Question::updateById($data,$params['id']);
        if (empty($rs)) {
            $result['code'] = '100004';
            $result['msg'] = '修改失败';
        }
        return $result;
    }
}
