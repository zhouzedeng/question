<?php
namespace Wangxun\Question\Service;

use Wangxun\Question\Model\Prize;

/**
 * CutService
 * Class CutService
 * @package Wangxun\Common\Service
 * @author yanguang
 * @since 2018-11-22
 */
class PrizeService extends BaseService
{
    /**
     * 获取列表数据
     * @param array $data
     * @return array
     * @author Zed
     * @since 2018-11-6
     */
    public static function getList($data = [])
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());

        // 查询数据
        $param = [];
        $order = array('id' , 'desc');
        $list = Prize::getListByParam($param, $data['page'], $data['limit'], null, $order);
        $total = Prize::getCntByParam($param);
        foreach ($list as $k => $v) {
            $list[$k]->time = date("Y-m-d H:i:s", $v->time);
            $list[$k]->updated_at = date("Y-m-d H:i:s", $v->updated_at);
        }
        $result['data'] = $list;
        $result['count'] = $total;
        return $result;
    }
}
