<?php
namespace Wangxun\Question\Service;

use Wangxun\Question\Model\Title;


/**
 * 称号业务
 * Class QuestionService
 * @package Wangxun\Common\Service
 * @author guang
 * @since 2018-12-3
 */
class TitleService extends BaseService
{
    /**
     * 获取称号内容
     * @param array $data
     * @return array
     * @author yanguang
     * @since 2018-12-3
     */
    public static function getList($data = [])
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());

        // 查询数据

        $param = [];
        $order = array('id' , 'desc');
        $param['delete_at'] = 0;
        $list = Title::getListByParam( $param,$data['page'], $data['limit'], null, $order);
        $total = Title::getCntByParam($param);
        foreach ($list as $k => $v) {
            $list[$k]->time = date("Y-m-d H:i:s", $v->created_at);
            $list[$k]->updatetime = date("Y-m-d H:i:s", $v->updated_at);
        }
        $result['data'] = $list;
        $result['count'] = $total;
        return $result;
    }
    /**
     * 获取一条称号数据
     * @param array $data
     * @return array
     * @author shengquan
     * @since 2018-11-1
     */
    public static function getFind($params = [])
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        // 查询数据
        $where = [];
        $where ['id'] = $params ['id'];
        $find = Title::getOneByParam($where,'*');
        $result['data'] = $find;
        return $result;
    }
    /**
     * 新增称号
     * @param array $params
     * @return array
     * @author guang
     * @since 2018-12-1
     */
    public static function save($params = array())
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        $data = [
            'score_min' => $params['score_min'],
            'score_max' => $params['score_max'],
            'title' => $params['title'],
            'created_at' => time(),
            'updated_at' => time()
        ];
        $rs = Title::add($data);
        if (empty($rs)) {
            $result['code'] = '200001';
            $result['msg'] = '添加失败';
        }
        return $result;
    }
    /**
     * 删除称号
     * @param array $params
     * @return array
     * @author shengquan
     * @since 2018-11-1
     */
    public static function del($params = array())
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        $data ['delete_at'] = time();
        $rs = Title::updateById($data,$params['id']);
        if (empty($rs)) {
            $result['code'] = '100001';
            $result['msg'] = '删除失败';
        }
        return $result;
    }

    /**
     * 修改称号数据
     * @param array $params
     * @return array
     * @author guang
     * @since 2018-12-3
     */
    public static function updata_title($params = array())
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());
        $data = [
            'score_min' => $params['score_min'],
            'score_max' => $params['score_max'],
            'title' => $params['title'],
            'updated_at' => time()
        ];
        $rs = Title::updateById($data,$params['id']);
        if (empty($rs)) {
            $result['code'] = '100004';
            $result['msg'] = '修改失败';
        }
        return $result;
    }
}
