<?php
namespace Wangxun\Question\Service;

use Wangxun\Question\Model\UserPrize;

/**
 * CutService
 * Class CutService
 * @package Wangxun\Common\Service
 * @author yanguang
 * @since 2018-11-22
 */
class UserPrizeService extends BaseService
{
    /**
     * 获取列表数据
     * @param array $data
     * @return array
     * @author Zed
     * @since 2018-11-6
     */
    public static function getList($data = [])
    {
        $result = array('code' => 0,  'msg' => '', 'data' => array());

        // 查询数据
        $param = [];
        $param['deleted_at'] = 0;
        $order = array('id' , 'desc');
        $list = UserPrize::getListByParam($param, $data['page'], $data['limit'], null, $order);
        $total = UserPrize::getCntByParam($param);
        foreach ($list as $k => $v) {
            $list[$k]->created_at = date("Y-m-d H:i:s", $v->created_at);
        }
        $result['data'] = $list;
        $result['count'] = $total;
        return $result;
    }
}
