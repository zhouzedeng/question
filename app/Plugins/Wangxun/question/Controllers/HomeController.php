<?php
namespace Wangxun\Question\Controllers;

/**
 * 首页控制器
 * Class HomeController
 * @package Wangxun\Activity\Controllers
 * @author Zed
 * @since 2018-11-1
 */
class HomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $this->checkPermission();
        return view('wangxun.question.home.index');
    }

    /**
     * getToken
     * @return string
     */
    protected function getToken()
    {
        $this->checkPermission();
        return md5(uniqid() . uniqid());
    }
}

