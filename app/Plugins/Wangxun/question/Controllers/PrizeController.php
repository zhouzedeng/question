<?php
namespace Wangxun\Question\Controllers;

use Illuminate\Http\Request;
use Wangxun\Question\Service\UserPrizeService;
use Wangxun\Question\Service\PrizeService;

/**
 * PrizeController
 * Class PrizeController
 * @package Wangxun\Activity\Controllers
 * @author yanguang
 * @since 2018-11-22
 */
class PrizeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 奖品列表页
     * @author Zed
     * @since 2018-11-6
     */
    public function index()
    {
        return view('wangxun.question.prize.index');
    }

    /**
     * 获取奖品列表数据
     * @author Zed
     * @since 2018-11-6
     */
    public function getList( )
    {
        $this->checkPermission();
        $result = PrizeService::getList($this->params);
        return $result;
    }

    /**
     * 添加奖品页
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $this->checkPermission();
        if ($request->isMethod('post')){
            $this->setSellerToParams($request);
            $params = $this->params;
            if (empty($params['prize_name'])) {
                return $this->apiFail('100001', '奖品内容必填');
            }
            if (empty($params['integral'])) {
                return $this->apiFail('100003', '选项一');
            }
            if (empty($params['explain'])) {
                return $this->apiFail('100004', '选项二');
            }

            $result = PrizeService::save($params);
            return $result;
        }
        return view('wangxun.question.prize.add');
    }

    /**
     * 修改奖品页
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $this->checkPermission();
        $params = $this->params;
        if($request->isMethod('post')){
            if (empty($params['prize_name'])) {
                return $this->apiFail('100001', '奖品名称必填');
            }
            if (empty($params['integral'])) {
                return $this->apiFail('100001', '奖品兑换所需积分必填');
            }
            if (empty($params['explain'])) {
                return $this->apiFail('100001', '奖品简介必填');
            }

            $result =  PrizeService::update($params);
            return $result;
        }
        $result = PrizeService::getFind($params);
        return view('wangxun.question.prize.edit',['prize'=>$result['data']]);
    }

    /**
     * 删除奖品
     *  @return \Illuminate\Http\JsonResponse
     */
    public function del()
    {
        $this->checkPermission();
        $params = $this->params;
        if (empty($params['id'])) {
            return $this->apiFail('100001', '题目ID必填');
        }
        $result = PrizeService::del($params);
        return $result;
    }
}

