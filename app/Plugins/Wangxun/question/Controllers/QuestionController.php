<?php
namespace Wangxun\Question\Controllers;

use Illuminate\Http\Request;
use Wangxun\Question\Service\QuestionService;



/**
 * 题目控制器
 * Class UserController
 * @package App\Http\Controllers
 * @author guang
 * @since 2018-11-1
 */
class QuestionController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 列表页
     * @author guang
     * @since 2018-11-1
     */
    public function index( )
    {
        $this->checkPermission();
        return view('wangxun.question.question.index');
    }

    /**
     * 添加页
     * @author guang
     * @since 2018-11-1+
     */
    public function add()
    {
        $this->checkPermission();
        return view('wangxun.question.question.add');
    }

    /**
     * 获取题目接口
     * @author guang
     * @since 2018-11-1
     */
    public function getList( )
    {
        $this->checkPermission();
        $result = QuestionService::getList($this->params);
        return $result;
    }

    /**
     * 新增题目接口
     * @author guang
     * @since 2018-11-1
     */
    public function save( )
    {
        $this->checkPermission();
        $params = $this->params;
        if (empty($params['question'])) {
            return $this->apiFail('1000021', '题目内容必填');
        }
        if (empty($params['option_1'])) {
            return $this->apiFail('100003', '选项一');
        }
        if (empty($params['option_2'])) {
            return $this->apiFail('100004', '选项二');
        }
        if (empty($params['option_3'])) {
            return $this->apiFail('100005', '选项三');
        }
        if (empty($params['option_4'])) {
            return $this->apiFail('100006', '选项四');
        }
        if (empty($params['answer'])) {
            return $this->apiFail('100002', '答案必填');
        }

        $result = QuestionService::save($params);
        return $result;
    }

    /**
     * 删除题目
     *  @return \Illuminate\Http\JsonResponse
     */
    public function del()
    {
        $this->checkPermission();
        $params = $this->params;
        if (empty($params['id'])) {
            return $this->apiFail('100001', '题目ID必填');
        }
        $result = QuestionService::del($params);
        return $result;
    }

    /**
    * 编辑页面
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function edit(Request $request)
    {
        $this->checkPermission();
        $params = $this->params;
//      return($params);
        if($request->isMethod('post')){
            if (empty($params['question'])) {
                return $this->apiFail('100001', '题目内容必填');
            }
            if (empty($params['option_1'])) {
                return $this->apiFail('100001', '选项一必填');
            }
            if (empty($params['option_2'])) {
                return $this->apiFail('100001', '选项二必填');
            }
            if (empty($params['option_3'])) {
                return $this->apiFail('100001', '选项三必填');
            }
            if (empty($params['option_4'])) {
                return $this->apiFail('100001', '选项四必填');
            }
            if (empty($params['answer'])) {
                return $this->apiFail('100002', '答案必填');
            }

            $result =  QuestionService::updata_question($params);
            return $result;
        }
        $result = QuestionService::getFind($params);
        return view('wangxun.question.question.edit',['question'=>$result['data']]);
    }

}

