<?php
namespace Wangxun\Question\Controllers;

use Illuminate\Http\Request;
use Wangxun\Question\Service\TitleService;



/**
 * 称号控制器
 * Class UserController
 * @package App\Http\Controllers
 * @author guang
 * @since 2018-12-3
 */
class TitleController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 列表页
     * @author guang
     * @since 2018-11-1
     */
    public function index()
    {
        $this->checkPermission();
        return view('wangxun.question.title.index');
    }

    /**
     * 添加页
     * @author guang
     * @since 2018-11-1+
     */
    public function add()
    {
        $this->checkPermission();
        return view('wangxun.question.title.add');
    }

    /**
     * 删除称号
     * @return \Illuminate\Http\JsonResponse
     */
    public function del()
    {
        $this->checkPermission();
        $params = $this->params;
        $result = TitleService::del($params);
        return $result;
    }

    /**
     * 获取称号接口
     * @author guang
     * @since 2018-12-3
     */
    public function getList( )
    {
        $this->checkPermission();
        $result = TitleService::getList($this->params);
        return $result;
    }

    /**
     * 新增称号接口
     * @author guang
     * @since 2018-12-1
     */
    public function save( )
    {
        $this->checkPermission();
        $params = $this->params;
        if (empty($params['score_min'])) {
            return $this->apiFail('1000021', '请输入最小分数');
        }
        if (empty($params['score_max'])) {
            return $this->apiFail('1000021', '请输入最大分数');
        }
        if (empty($params['title'])) {
            return $this->apiFail('1000021', '称号必填');
        }

        $result = TitleService::save($params);
        return $result;
    }

    /**
     * 编辑页面
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $this->checkPermission();
        $params = $this->params;
        if ($request->isMethod('post')) {
            if (empty($params['score_min'])) {
                return $this->apiFail('1000021', '请输入最小分数');
            }
            if (empty($params['score_max'])) {
                return $this->apiFail('1000021', '请输入最大分数');
            }
            if (empty($params['title'])) {
                return $this->apiFail('100001', '称号必填');
            }
            $result = TitleService::updata_title($params);
            return $result;
        }
        $result = TitleService::getFind($params);
        return view('wangxun.question.title.edit', ['title' => $result['data']]);
    }
}

