<?php
namespace Wangxun\Question\Controllers;

use Wangxun\Question\Service\UserPrizeService;

/**
 * CutController
 * Class CutController
 * @package Wangxun\Activity\Controllers
 * @author yanguang
 * @since 2018-11-22
 */
class UserPrizeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 用户兑换奖品列表页
     */
    public function index()
    {
        $this->checkPermission();
        return view('wangxun.question.userprize.index');
    }

    /**
     * 获取户兑换奖品列表数据
     */
    public function getList( )
    {
        $this->checkPermission();
        $result = UserPrizeService::getList($this->params);
        return $result;
    }


}

