<?php
namespace Wangxun\Question\Controllers;

use Wangxun\Question\Service\VerifyService;

/**
 * 活动控制器
 * Class UserController
 * @package App\Http\Controllers
 * @author Zed
 * @since 2018-11-3
 */
class VerifyController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 列表页
     * @author Zed
     * @since 2018-11-3
     */
    public function index()
    {
        $this->checkPermission();
        return view('wangxun.question.verify.index');
    }

    /**
     * 获取活动接口
     * @author Zed
     * @since 2018-11-3
     */
    public function getList()
    {
        $this->checkPermission();
        $result = VerifyService::getCheckActivityList($this->params);
        return $result;
    }

    /**
     * 审核
     * @return array
     * @author Zed
     * @since 2018-11-3
     */
    public function check()
    {
        $this->checkPermission();
        $result = VerifyService::check($this->params);
        return $result;
    }
}

