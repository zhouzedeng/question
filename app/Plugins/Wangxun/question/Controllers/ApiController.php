<?php
namespace Wangxun\Question\Controllers;

use Wangxun\Question\Service\SeriesService;
use Wangxun\Question\Service\ApiService;
use Wangxun\Question\Service\ThirdApiService;

/**
 * ApiController
 * Class ApiController
 * @package Wangxun\Activity\Controllers
 * @author shengquan
 * @since 2018-11-7
 */
class ApiController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 新增用户接口
     * @author shengquan
     * @since 2018-11-7
     */
    public function addUser()
    {
        $result = ApiService::addUser($this->params);
        return $result;
    }
    /**
     * 获取某经销商某个活动下的商品列表
     * @author shengquan
     * @since 2018-11-7
     */
    public function getAllSellerGoods()
    {
        $result = ApiService::getAllSellerGoods($this->params);
        return $result;

    }
    /**
     * 用户添加砍价商品（用户点击某一商品的“立即砍价”按钮）
     * @author shengquan
     * @since 2018-11-7
     */
    public function addGoodsToCut()
    {
        $result = ApiService::addGoodsToCut($this->params);
        return $result;

    }
    /**
     * 通过砍价ID获取活动信息 、 商品信息 、 砍价详情
     * @author shengquan
     * @since 2018-11-7
     */
    public function getCutInfo()
    {
        $result = ApiService::getCutInfo($this->params);
        return $result;

    }
    /**
     * 获取砍价好友榜
     * @author shengquan
     * @since 2018-11-7
     */
    public function getCutVisitor()
    {
        $result = ApiService::getCutVisitor($this->params);
        return $result;

    }

    /**
     * 好友砍价
     * @author shengquan
     * @since 2018-11-7
     */
    public function cut()
    {
        $result = ApiService::cut($this->params);
        return $result;

    }
    /**
     * 通过活动ID和seller_id获取活动详情
     * @author shengquan
     * @since 2018-11-8
     */
    public function getActivity()
    {
        $result = ApiService::getActivity($this->params);
        return $result;

    }

    /**
     *  获取车系
     * @author shengquan
     * @since 2018-11-14
     */
    public function getSeries()
    {
        $result = ThirdApiService::getCarSeriesInfo($this->params);
        SeriesService::save($result['data']);
        $result = ApiService::getSeries($this->params);
        return $result;
    }

    /**
     * 获取我的奖品明细
     * @author yanguang
     * @since 2018-11-17
     */
    public function getMyPrizeList()
    {
        $result = ApiService::getMyPrizeList($this->params);
        return $result;

    }
    /**
     * 获取题目
     * @author yanguang
     * @since 2018-11-17
     */
    public function getQuestion()
    {
        $result = ApiService::getQuestion($this->params);
        return $result;

    }
    /**
     * 获取奖品列表
     * @author yanguang
     * @since 2018-11-17
     */
    public function getPrizeList()
    {
        $result = ApiService::getPrizeList($this->params);
        return $result;

    }
    /**
     * 兑换奖品
     * @author yanguang
     * @since 2018-11-17
     */
    public function getExchange()
    {
        $result = ApiService::getExchange($this->params);
        return $result;

    }
    /**
     * 获取奖品详情
     * @author yanguang
     * @since 2018-11-17
     */
    public function getPrize()
    {
        $result = ApiService::getPrize($this->params);
        return $result;

    }
    /**
     * 新增答题用户接口
     * @author yanguang
     * @since 2018-11-17
     */
    public function addQuestionUser()
    {
        $result = ApiService::addQuestionUser($this->params);
        return $result;

    }
    /**
     * 新增答题用户接口
     * @author yanguang
     * @since 2018-12-2
     */
    public function getRule()
    {
        $result = ApiService::getRule($this->params);
        return $result;

    }
    /**
     * 答题结束获得对应奖品
     * @author yanguang
     * @since 2018-12-2
     */
    public function getSettlement()
    {
        $result = ApiService::getSettlement($this->params);
        return $result;

    }
    /**
     * 答题结束获得对应称号
     * @author yanguang
     * @since 2018-12-2
     */
    public function getTitle()
    {
        $result = ApiService::getTitle($this->params);
        return $result;

    }

}

