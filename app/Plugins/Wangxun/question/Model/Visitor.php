<?php
namespace Wangxun\Question\Model;

/**
 * Class Visitor
 * @package Wangxun\Common\Model
 * @author quan
 * @since 2018-11-7
 */
class Visitor extends Base
{
    const ID = 'id';
    const TABLE = 'wangxun_question_visitor';
}
