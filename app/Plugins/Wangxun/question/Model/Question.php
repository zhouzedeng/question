<?php
namespace Wangxun\Question\Model;

/**
 * 题目表
 * Class Activity
 * @package Wangxun\Common\Model
 * @author yanguang
 * @since 2018-11-22
 */
class Question extends Base
{
    const ID = 'id';
    const TABLE = 'wangxun_question_question';
}
