<?php
namespace Wangxun\Question\Model;

/**
 * Class User
 * @package Wangxun\Common\Model
 * @author Zed
 * @since 2018-10-30
 */
class User extends Base
{
    const ID = 'id';
    const TABLE = 'wangxun_question_users';
}
