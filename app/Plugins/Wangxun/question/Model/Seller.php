<?php
namespace Wangxun\Question\Model;

/**
 * 经销商
 * Class Seller
 * @package Wangxun\Common\Model
 * @author Zed
 * @since 2018-10-30
 */
class Seller extends Base
{
    const ID = 'id';
    const TABLE = 'wangxun_question_seller';
}
