<?php
namespace Wangxun\Question\Model;

/**
 * 砍价表
 * Class Cut
 * @package Wangxun\Common\Model
 * @author yanguang
 * @since 2018-11-22
 */
class UserPrize extends Base
{
    const ID = 'id';
    const TABLE = 'wangxun_question_user_prize';
}
