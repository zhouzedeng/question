<?php
namespace Wangxun\Question\Model;

/**
 * Class Series
 * @package Wangxun\Common\Model
 */
class Series extends Base
{
    const ID = 'id';
    const TABLE = 'wangxun_question_series';
}
